In order to run the program please firstly get python2.7 and pip:
        $ apt-get install python python-pip

Then install all the dependence with the next command:
        $ pip install -r requirement.txt

Finally run the following command to launch the programm:
        $ python main.py [port address]
