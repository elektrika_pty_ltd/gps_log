import pynmea2
import serial
import sys
import select

if len(sys.argv) != 2:
    print 'Please provide the serial port of the GPS (ex: \'/dev/tty.usbserial\').'
    sys.exit()    

ser = serial.Serial() # Open serial port
ser.baudrate = 4800
ser.port = str(sys.argv[1])
try:
    ser.open()
    print 'Connected to: ' + ser.port
    ser.is_open
except:
    print 'Could not open port: ' + ser.port
    sys.exit()    

f = open('log.txt', 'a') # Open file for writing into it. 
print 'Writing to the file ...'

read_list = [sys.stdin] # files monitored for input

while True:
    try:
        frame = ser.readline() # read a '\n' terminated line
        if frame[3:6] == 'RMC':
            parsed = pynmea2.parse(frame)
            temp = str(parsed.timestamp) + ' | ' + str(parsed.latitude) + ',' + str(parsed.longitude) + ' | ' + str(parsed.spd_over_grnd)
            f.write(temp + '\n')
            print temp

        buffer = select.select(read_list, [], [], 0.1)[0]
        if buffer:
            for file in buffer:
                input = file.readline()
                if not input: # EOF, remove file from input list
                    read_list.remove(file)
                elif input.rstrip(): # optional: skipping empty lines
                    f.write(input)

    except KeyboardInterrupt:
        break

f.close() # close and free up system resources
ser.close()
